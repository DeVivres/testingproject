﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Common.DTO
{
    public class TaskStateDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
