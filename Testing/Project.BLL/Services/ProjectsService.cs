﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.BLL.Services
{
    public class ProjectsService : IService<ProjectDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(ProjectDTO item)
        {
            var mapped = _mapper.Map<DAL.Entities.Project>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            _unitOfWork.Projects.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var result = _unitOfWork.Projects.Delete(id);
            _unitOfWork.SaveChanges();
            return result;
        }

        public ProjectDTO Get(int id)
        {
            var result = _unitOfWork.Projects.Get(id);
            return _mapper.Map<ProjectDTO>(result);
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            var result = _unitOfWork.Projects.GetAll();
            return _mapper.Map<IEnumerable<ProjectDTO>>(result);
        }

        public bool Update(ProjectDTO item)
        {
            var status = false;
            var mapped = _mapper.Map<DAL.Entities.Project>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);
            
            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Projects.Update(mapped);

                _unitOfWork.SaveChanges();
            }
            return status;
        }
    }
}
