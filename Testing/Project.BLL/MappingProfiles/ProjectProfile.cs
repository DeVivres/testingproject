﻿using AutoMapper;
using Project.Common.DTO;

namespace Project.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<DAL.Entities.Project, ProjectDTO>().ReverseMap();
        }
    }
}
