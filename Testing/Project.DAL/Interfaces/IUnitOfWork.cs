﻿using Project.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ProjectsRepository Projects { get; }
        TasksRepository Tasks { get; }
        TaskStatesRepository States { get; }
        TeamsRepository Teams { get; }
        UsersRepository Users { get; }
        int SaveChanges();
    }
}
