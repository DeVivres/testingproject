﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Project.DAL.Migrations
{
    public partial class AddedSimpleSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "IncomeFromProject", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2020, 7, 1, 6, 15, 53, 288, DateTimeKind.Local).AddTicks(5115), new DateTime(2021, 2, 2, 2, 57, 55, 67, DateTimeKind.Local).AddTicks(7911), @"Ea ab omnis saepe rem vel et.
Illo quaerat eos accusantium reiciendis dolores quibusdam ratione.", 0, "Expedita amet quas id a.", 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(2020, 7, 1, 1, 22, 9, 903, DateTimeKind.Local).AddTicks(937), "DoTheJob", new DateTime(2020, 12, 11, 0, 30, 51, 72, DateTimeKind.Local).AddTicks(4501), "ToDo", 1, 1, 1 });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2020, 6, 30, 21, 46, 57, 1, DateTimeKind.Local).AddTicks(953), "Depits" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 1, new DateTime(2005, 10, 29, 0, 26, 53, 19, DateTimeKind.Local).AddTicks(3606), "trolson@gmail.com", "David", "Trollson", new DateTime(2020, 6, 24, 12, 11, 10, 569, DateTimeKind.Local).AddTicks(1173), 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
