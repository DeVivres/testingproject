﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.DAL.Repositories
{
    public class TaskStatesRepository : IRepository<TaskState>
    {
        private readonly DatabaseContext _context;

        public TaskStatesRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void Create(TaskState item) => _context.States.Add(item);

        public bool Delete(int id)
        {
            var item = _context.States.FirstOrDefault(s => s.Id == id);

            if (item == null) return false;

            _context.States.Remove(item);
            return true;
        }

        public TaskState Get(int id) => _context.States.FirstOrDefault(s => s.Id == id);
        public IEnumerable<TaskState> GetAll() => _context.States;

        public bool Update(TaskState item)
        {
            var exists = _context.States.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
