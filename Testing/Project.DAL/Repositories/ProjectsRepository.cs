﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.DAL.Repositories
{
    public class ProjectsRepository : IRepository<Entities.Project>
    {
        private readonly DatabaseContext _context;

        public ProjectsRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void Create(Entities.Project item) => _context.Projects.Add(item);

        public bool Delete(int id)
        {
            var item = _context.Projects.FirstOrDefault(p => p.Id == id);

            if (item == null) return false;

            _context.Projects.Remove(item);
            return true;
        }

        public Entities.Project Get(int id) => _context.Projects.FirstOrDefault(u => u.Id == id);
        public IEnumerable<Entities.Project> GetAll() => _context.Projects;

        public bool Update(Entities.Project item)
        {
            var exists = _context.Projects.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
