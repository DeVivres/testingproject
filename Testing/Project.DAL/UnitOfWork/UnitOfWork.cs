﻿using Project.DAL.Context;
using Project.DAL.Interfaces;
using Project.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;
        private bool _disposed;

        private ProjectsRepository _projectsRepository;
        private TasksRepository _tasksRepository;
        private TaskStatesRepository _statesRepository;
        private TeamsRepository _teamsRepository;
        private UsersRepository _usersRepository;

        public UnitOfWork(DatabaseContext context)
        {
            _context = context;
            _disposed = false;
        }

        public ProjectsRepository Projects
        {
            get
            {
                if (_projectsRepository == null) _projectsRepository = new ProjectsRepository(_context);

                return _projectsRepository;
            }
        }

        public TasksRepository Tasks
        {
            get
            {
                if (_tasksRepository == null) _tasksRepository = new TasksRepository(_context);

                return _tasksRepository;
            }
        }

        public TaskStatesRepository States
        {
            get
            {
                if (_statesRepository == null) _statesRepository = new TaskStatesRepository(_context);

                return _statesRepository;
            }
        }

        public TeamsRepository Teams
        {
            get
            {
                if (_teamsRepository == null) _teamsRepository = new TeamsRepository(_context);

                return _teamsRepository;
            }
        }

        public UsersRepository Users
        {
            get
            {
                if (_usersRepository == null) _usersRepository = new UsersRepository(_context);

                return _usersRepository;
            }
        }

        public int SaveChanges() => _context.SaveChanges();

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
