﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.DAL.Context
{
        public static class ModelBuilderExtensionsx
        {
            public static void Seed(this ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<Entities.Project>().HasData(
                    new Entities.Project
                    {
                        Id = 1,
                        Name = "Expedita amet quas id a.",
                        Description = "Ea ab omnis saepe rem vel et.\nIllo quaerat eos accusantium reiciendis dolores quibusdam ratione.",
                        CreatedAt = Convert.ToDateTime("2020-07-01T03:15:53.2885115+00:00"),
                        Deadline = Convert.ToDateTime("2021-02-02T00:57:55.0677911+00:00"),
                        AuthorId = 1,
                        TeamId = 1
                    });
                modelBuilder.Entity<Task>().HasData(
                    new Task
                    {
                        Id = 1,
                        Name = "ToDo",
                        Description = "DoTheJob",
                        CreatedAt = Convert.ToDateTime("2020-06-30T22:22:09.9030937+00:00"),
                        FinishedAt = Convert.ToDateTime("2020-12-10T22:30:51.0724501+00:00"),
                        State = 1,
                        ProjectId = 1,
                        PerformerId = 1
                    });
                modelBuilder.Entity<Team>().HasData(
                   new Team
                   {
                       Id = 1,
                       Name = "Depits",
                       CreatedAt = Convert.ToDateTime("2020-06-30T18:46:57.0010953+00:00")
                   });
                modelBuilder.Entity<User>().HasData(
                   new User
                   {
                       Id = 1,
                       FirstName = "David",
                       LastName = "Trollson",
                       Email = "trolson@gmail.com",
                       BirthDay = Convert.ToDateTime("2005-10-28T21:26:53.0193606+00:00"),
                       RegisteredAt = Convert.ToDateTime("2020-06-24T09:11:10.5691173+00:00"),
                       TeamId = 1
                   });
            }
        }
    }
