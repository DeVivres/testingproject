﻿using Microsoft.AspNetCore.Mvc;
using Project.BLL.Interfaces;
using Project.Common.DTO;

namespace Project.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _projecsService;

        public ProjectsController(IService<ProjectDTO> projecsService)
        {
            _projecsService = projecsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _projecsService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _projecsService.Get(id);

            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] ProjectDTO project)
        {
            var result = _projecsService.Create(project);

            if (!result)
            {
                return BadRequest(project);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] ProjectDTO project)
        {
            var result = _projecsService.Update(project);

            if (!result)
            {
                return BadRequest(project);
            }
            return Ok(project);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _projecsService.Delete(id);

            if (!result)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
