﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project.BLL.Interfaces;
using Project.Common.DTO;

namespace Project.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IService<TaskDTO> _tasksService;

        public TasksController(IService<TaskDTO> tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _tasksService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _tasksService.Get(id);

            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskDTO task)
        {
            var result = _tasksService.Create(task);

            if (!result)
            {
                return BadRequest(task);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] TaskDTO task)
        {
            var result = _tasksService.Update(task);

            if (!result)
            {
                return BadRequest(task);
            }
            return Ok(task);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _tasksService.Delete(id);

            if (!result)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
