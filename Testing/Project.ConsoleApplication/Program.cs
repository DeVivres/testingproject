﻿using Project.ConsoleApplication.Services;
using System;

namespace Project.ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new QueryService();
            var result1 = service.GetUserTasksInProject(1);
            var result2 = service.GetUserTasksWithLength(4, 45);
            var result3 = service.GetUserFinishedTasks(5, 2020);
            var result4 = service.GetUsersMinAge(10);
            var result5 = service.GetSortedUsersWithTasks();
            foreach (var item in result1)
            {
                Console.WriteLine(item);
            }
        }
    }
}
